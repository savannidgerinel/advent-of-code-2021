use crate::errors::Error;

const DATA: &str = include_str!("../data/03.txt");

fn diagnostics() -> Report {
    let records = DATA.lines().map(Bitfield::from).collect::<Vec<Bitfield>>();
    Report {
        field_size: DATA.lines().next().unwrap().len(),
        records,
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
struct Bitfield(usize, u64);

impl Bitfield {
    fn set(&self, bit: usize) -> Bitfield {
        Bitfield(self.0, self.1 | 1 << bit)
    }

    fn clear(&self, bit: usize) -> Bitfield {
        Bitfield(self.0, self.1 & !(1 << bit))
    }

    fn get(&self, bit: usize) -> bool {
        (self.1 >> bit & 1) == 1
    }

    fn invert(&self) -> Bitfield {
        let bits = !self.1 & (2_u64.pow(self.0 as u32) - 1);
        Bitfield(self.0, bits)
    }
}

impl From<&str> for Bitfield {
    fn from(s: &str) -> Bitfield {
        Bitfield(s.len(), u64::from_str_radix(s, 2).unwrap())
    }
}

struct Report {
    field_size: usize,
    records: Vec<Bitfield>,
}

fn most_common_val<'a>(records: impl Iterator<Item = &'a Bitfield>, bit: usize) -> bool {
    let mut zero_cnt = 0;
    let mut one_cnt = 0;
    for record in records {
        if record.get(bit) {
            one_cnt += 1;
        } else {
            zero_cnt += 1;
        }
    }
    one_cnt >= zero_cnt
}

fn gamma(report: &Report) -> u64 {
    let mut gamma_field = Bitfield(report.field_size, 0);
    for i in 0..report.field_size {
        if most_common_val(report.records.iter(), i) {
            gamma_field = gamma_field.set(i);
        }
    }
    gamma_field.1
}

fn epsilon(report: &Report) -> u64 {
    Bitfield(report.field_size, gamma(&report)).invert().1
}

fn life_support_filter<'a>(
    records: impl Iterator<Item = &'a Bitfield>,
    bit: usize,
    desired: bool,
) -> Vec<&'a Bitfield> {
    records.filter(move |rec| rec.get(bit) == desired).collect()
}

fn o2_gen_<'a>(records: impl Iterator<Item = &'a Bitfield> + Clone, bit: usize) -> Bitfield {
    let mcv = most_common_val(records.clone(), bit);
    let records = life_support_filter(records, bit, mcv);
    if records.len() == 1 {
        return (*records.first().unwrap()).clone();
    };
    return o2_gen_(records.iter().cloned(), bit - 1);
}

fn o2_gen(report: &Report) -> u64 {
    o2_gen_(report.records.iter(), report.field_size - 1).1
}

fn co2_scrub_<'a>(records: impl Iterator<Item = &'a Bitfield> + Clone, bit: usize) -> Bitfield {
    let mcv = most_common_val(records.clone(), bit);
    let records = life_support_filter(records, bit, !mcv);
    if records.len() == 1 {
        return (*records.first().unwrap()).clone();
    };
    return co2_scrub_(records.iter().cloned(), bit - 1);
}

fn co2_scrub(report: &Report) -> u64 {
    co2_scrub_(report.records.iter(), report.field_size - 1).1
}

pub fn day3a() -> Result<String, Error> {
    let report = diagnostics();
    let gamma = gamma(&report);
    let epsilon = epsilon(&report);
    Ok(format!("day3a: {}", gamma * epsilon))
}

pub fn day3b() -> Result<String, Error> {
    let report = diagnostics();
    let o2gen = o2_gen(&report);
    let co2scrub = co2_scrub(&report);
    Ok(format!("day3b: {}", o2gen * co2scrub))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn it_populates_bitfield_from_str() {
        assert_eq!(Bitfield::from("10011"), Bitfield(5, 19));
    }

    #[test]
    fn bit_ops() {
        assert_eq!(Bitfield(5, 19).get(4), true);
        assert_eq!(Bitfield(5, 19).get(3), false);
        assert_eq!(Bitfield(5, 19).get(2), false);
        assert_eq!(Bitfield(5, 19).get(1), true);
        assert_eq!(Bitfield(5, 19).get(0), true);

        assert_eq!(Bitfield(5, 19).set(3), Bitfield(5, 27));
        assert_eq!(Bitfield(5, 19).set(3).clear(3), Bitfield(5, 19));
        assert_eq!(Bitfield(5, 22).invert(), Bitfield(5, 9));
    }

    const TEST_DATA: &str = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
";

    #[test]
    fn calculates_gamma_from_test_data() {
        let records: Vec<Bitfield> = TEST_DATA.lines().map(Bitfield::from).collect();
        let report = Report {
            field_size: TEST_DATA.lines().next().unwrap().len(),
            records,
        };
        assert_eq!(gamma(&report), 22);
    }

    #[test]
    fn calculates_epsilon_from_test_data() {
        let records: Vec<Bitfield> = TEST_DATA.lines().map(Bitfield::from).collect();
        let report = Report {
            field_size: TEST_DATA.lines().next().unwrap().len(),
            records,
        };
        assert_eq!(epsilon(&report), 9);
    }

    #[test]
    fn calculates_o2_gen() {
        let records: Vec<Bitfield> = TEST_DATA.lines().map(Bitfield::from).collect();
        let report = Report {
            field_size: TEST_DATA.lines().next().unwrap().len(),
            records,
        };
        assert_eq!(o2_gen(&report), 23);
    }

    #[test]
    fn calculates_co2_scrubber() {
        let records: Vec<Bitfield> = TEST_DATA.lines().map(Bitfield::from).collect();
        let report = Report {
            field_size: TEST_DATA.lines().next().unwrap().len(),
            records,
        };
        assert_eq!(co2_scrub(&report), 10);
    }
}
