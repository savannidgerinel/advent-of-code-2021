use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("day is unknown: {0}")]
    InvalidDay(String),
}
