use crate::errors::Error;
use std::{
    collections::{HashMap, HashSet},
    iter::FromIterator,
};

const DATA: &str = include_str!("../data/08.txt");

fn ss_patterns() -> Vec<Vec<char>> {
    [
        "abcefg", "cf", "acdeg", "acdfg", "bcdf", "abdfg", "abdefg", "acf", "abcdefg", "abcdfg",
    ]
    .iter()
    .map(|v| v.chars().collect::<Vec<char>>())
    .collect::<Vec<Vec<char>>>()
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
struct Signal(Vec<char>);

impl Signal {
    fn len(&self) -> usize {
        self.0.len()
    }

    fn contains(&self, rside: &Signal) -> bool {
        rside.0.iter().all(|s| self.0.contains(s))
    }

    fn subtract(&self, rside: &Signal) -> Signal {
        let mut res = self.clone().0;
        rside
            .0
            .iter()
            .for_each(|segment| res.retain(|s| *s != *segment));
        Signal(res)
    }
}

#[derive(Clone, Debug)]
struct IO {
    inputs: Vec<Signal>,
    outputs: Vec<Signal>,

    digits: HashMap<Signal, u8>,
}

impl IO {
    fn output_digits(&self) -> Vec<u8> {
        self.outputs
            .iter()
            .map(|v| match v.len() {
                2 => 1,
                3 => 7,
                4 => 4,
                7 => 8,
                _ => 0,
            })
            .filter(|v| *v != 0)
            .collect::<Vec<u8>>()
    }

    fn output(&self) -> u32 {
        self.outputs
            .iter()
            .map(|v| self.digits.get(v).unwrap())
            .fold(0, |acc, digit| acc * 10 + *digit as u32)
    }

    fn all(&self) -> Vec<Signal> {
        let mut lst = self.inputs.clone();
        lst.append(&mut self.outputs.clone());
        lst.sort_unstable();
        lst.dedup();
        lst
    }

    fn lower_left(&self) -> Signal {
        self.eight().subtract(&self.nine())
    }

    fn zero(&self) -> Signal {
        self.all()
            .iter()
            .filter(|v| v.len() == 6)
            .filter(|v| **v != self.nine())
            .find(|v| v.contains(&self.one()))
            .unwrap()
            .clone()
    }

    fn one(&self) -> Signal {
        self.all().iter().find(|v| v.len() == 2).unwrap().clone()
    }

    fn two(&self) -> Signal {
        let ll = self.lower_left();
        self.all()
            .iter()
            .filter(|v| v.len() == 5)
            .find(|v| v.contains(&ll))
            .unwrap()
            .clone()
    }

    fn three(&self) -> Signal {
        let options = self
            .all()
            .iter()
            .filter(|v| v.len() == 5)
            .cloned()
            .collect::<Vec<Signal>>();
        options
            .iter()
            .find(|o| o.contains(&self.one()))
            .unwrap()
            .clone()
    }

    fn four(&self) -> Signal {
        self.all().iter().find(|v| v.len() == 4).unwrap().clone()
    }

    fn five(&self) -> Signal {
        self.all()
            .iter()
            .filter(|v| v.len() == 5)
            .filter(|v| **v != self.two())
            .filter(|v| **v != self.three())
            .next()
            .unwrap()
            .clone()
    }

    fn six(&self) -> Signal {
        self.all()
            .iter()
            .filter(|v| v.len() == 6)
            .filter(|v| **v != self.zero())
            .filter(|v| **v != self.nine())
            .next()
            .unwrap()
            .clone()
    }

    fn seven(&self) -> Signal {
        self.all().iter().find(|v| v.len() == 3).unwrap().clone()
    }

    fn eight(&self) -> Signal {
        self.all().iter().find(|v| v.len() == 7).unwrap().clone()
    }

    fn nine(&self) -> Signal {
        let options = self
            .all()
            .iter()
            .filter(|v| v.len() == 6)
            .cloned()
            .collect::<Vec<Signal>>();
        options
            .iter()
            .find(|o| o.contains(&self.three()))
            .unwrap()
            .clone()
    }

    fn decode_patterns(&mut self) {
        /*
           [..self.inputs, ..self.outputs]
           .iter()
           .flatten()
           .for_each(|signal| match signal.len() {
           2 => {
        // fill in possibilities... the signal must correspond to the wires c and f,
        // but it's not clear which ones it could be
        }
        3 => {
        // fill in possibilities... the signal must correspond to the wires a, c, and f,
        // but it's not clear which ones it could be
        }
        4 => {
        // fill in possibilities... the signal must correspond to the wires b, c, d, and f,
        // but it's not clear which ones it could be
        }
        7 => {}
        _ => (),
        })
        */
    }
}

/*
fn signals(input: &str) -> IResult<&str, Signal> {
Ok((input, Signal(input.chars().collect::<Vec<char>>())))
}

fn signal_set(input: &str) -> IResult<&str, Vec<Signal>> {
println!("signal_set: {}", input);
separated_list1(space1, signals)(input)
}

fn io(input: &str) -> IResult<&str, IO> {
let (input, (input_signals, output_signals)) =
separated_pair(signal_set, tag("|"), signal_set)(input)?;

println!("input signals:  {:?}", input_signals);
println!("output signals: {:?}", output_signals);

unimplemented!()
}

fn lines(input: &str) -> IResult<&str, Vec<IO>> {
separated_list1(newline, io)(input)
}
*/

fn parse_signals(input: &str) -> Signal {
    let mut s: Vec<char> = input.trim().chars().collect();
    s.sort_unstable();
    s.dedup();
    Signal(s)
}

fn parse_line(input: &str) -> IO {
    let parts = input.split("|").collect::<Vec<&str>>();
    let inputs = parts[0]
        .trim()
        .split(" ")
        .map(parse_signals)
        .collect::<Vec<Signal>>();
    let outputs = parts[1]
        .trim()
        .split(" ")
        .map(parse_signals)
        .collect::<Vec<Signal>>();

    let mut s = IO {
        inputs,
        outputs,
        digits: HashMap::new(),
    };

    s.digits.insert(s.zero(), 0);
    s.digits.insert(s.one(), 1);
    s.digits.insert(s.two(), 2);
    s.digits.insert(s.three(), 3);
    s.digits.insert(s.four(), 4);
    s.digits.insert(s.five(), 5);
    s.digits.insert(s.six(), 6);
    s.digits.insert(s.seven(), 7);
    s.digits.insert(s.eight(), 8);
    s.digits.insert(s.nine(), 9);

    s
}

fn data(input: &str) -> Vec<IO> {
    input.lines().map(parse_line).collect::<Vec<IO>>()
}

fn count_easily_identified_outputs(input: Vec<IO>) -> usize {
    input.iter().map(|io| io.output_digits().len()).sum()
}

fn sum_outputs(input: Vec<IO>) -> u32 {
    input.iter().map(|i| i.output()).sum()
}

pub fn day08a() -> Result<String, Error> {
    Ok(format!(
        "day08a: {}",
        count_easily_identified_outputs(data(DATA))
    ))
}

pub fn day08b() -> Result<String, Error> {
    Ok(format!("day08b: {}", sum_outputs(data(DATA))))
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &str =
        "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";

    fn parser_works() {
        let lines = data(TEST_DATA);
        assert!(false);
    }

    #[test]
    fn counts_easily_identified() {
        let io_sets = data(TEST_DATA);
        assert_eq!(count_easily_identified_outputs(io_sets), 26);
    }

    #[test]
    fn sums_output_values() {
        let io_sets = data(TEST_DATA);
        assert_eq!(sum_outputs(io_sets), 61229);
    }

    fn demo() {
        let text =
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";
        let io_sets = data(text);
        let io = io_sets[0].clone();
        println!("0: {:?}", io.zero());
        println!("1: {:?}", io.one());
        println!("2: {:?}", io.two());
        println!("3: {:?}", io.three());
        println!("4: {:?}", io.four());
        println!("5: {:?}", io.five());
        println!("6: {:?}", io.six());
        println!("7: {:?}", io.seven());
        println!("8: {:?}", io.eight());
        println!("9: {:?}", io.nine());
        assert!(false);
    }

    #[test]
    fn complete_output() {
        let text =
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";
        let io_sets = data(text);
        assert_eq!(io_sets[0].output(), 5353);
    }
}
