use crate::errors::Error;

const DATA_01: &str = include_str!("../data/01.txt");

fn depth_increases(depths: Vec<u32>) -> usize {
    depths.windows(2).filter(|w| w[1] > w[0]).count()
}

fn sum_windows(depths: Vec<u32>, size: usize) -> Vec<u32> {
    depths
        .windows(size)
        .map(|w| w.iter().fold(0, |a, b| a + b))
        .collect()
}

pub fn day1a() -> Result<String, Error> {
    let depths = DATA_01.lines().map(|l| l.parse::<u32>().unwrap()).collect();
    Ok(format!("day1a: {}", depth_increases(depths)))
}

pub fn day1b() -> Result<String, Error> {
    let depths = DATA_01.lines().map(|l| l.parse::<u32>().unwrap()).collect();
    Ok(format!(
        "day1b: {}",
        depth_increases(sum_windows(depths, 3))
    ))
}

#[cfg(test)]
mod test {
    use super::*;

    const DEMO_1A: &str = "199
200
208
210
200
207
240
269
260
263";

    #[test]
    fn it_measures_depth_increases() {
        let depths: Vec<u32> = DEMO_1A.lines().map(|l| l.parse::<u32>().unwrap()).collect();
        assert_eq!(depth_increases(depths.clone()), 7);
        assert_eq!(depth_increases(sum_windows(depths, 3)), 5);
    }
}
