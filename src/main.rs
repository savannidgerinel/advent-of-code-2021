use std::env;

mod day07;
mod day08;
mod day09;
mod day1;
mod day10;
mod day11;
mod day12;
mod day13;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
pub mod errors;
pub mod table;
mod utils;

fn main() {
    let args: Vec<String> = env::args().collect();
    let day = &args[1];

    let result = match day.as_str() {
        "1a" => day1::day1a(),
        "1b" => day1::day1b(),
        "2a" => day2::day2a(),
        "2b" => day2::day2b(),
        "3a" => day3::day3a(),
        "3b" => day3::day3b(),
        "4a" => day4::day4a(),
        "4b" => day4::day4b(),
        "5a" => day5::day5a(),
        "5b" => day5::day5b(),
        "6a" => day6::day6a(),
        "6b" => day6::day6b(),
        "7a" => day07::day07a(),
        "7b" => day07::day07b(),
        "8a" => day08::day08a(),
        "8b" => day08::day08b(),
        "9a" => day09::day09a(),
        "9b" => day09::day09b(),
        "10a" => day10::day10a(),
        "10b" => day10::day10b(),
        "11a" => day11::day11a(),
        "11b" => day11::day11b(),
        "12a" => day12::day12a(),
        "12b" => day12::day12b(),
        "13a" => day13::day13a(),
        "13b" => day13::day13b(),
        _ => Err(errors::Error::InvalidDay(String::from(day))),
    };

    match result {
        Ok(val) => println!("{}", val),
        Err(err) => println!("Error: {}", err),
    }
}
