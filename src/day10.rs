use crate::errors::Error;
use std::collections::HashMap;

const DATA: &str = include_str!("../data/10.txt");

enum IResult {
    Complete,
    Corrupted(char),
    Incomplete(Vec<char>),
}

fn match_pairs() -> HashMap<char, char> {
    let mut pairs = HashMap::new();
    pairs.insert('(', ')');
    pairs.insert('[', ']');
    pairs.insert('{', '}');
    pairs.insert('<', '>');
    pairs
}

fn parse_line(line: &str) -> IResult {
    let pairs = match_pairs();
    let mut stack = vec![];
    let mut invalid = None;
    for c in line.chars() {
        let top = stack.last().map(|c| *c);
        match c {
            '(' | '[' | '{' | '<' => stack.push(c),
            ')' | ']' | '}' | '>' => match top {
                None => unimplemented!(),
                Some(top) => {
                    let exp = pairs.get(&top).map(|c| *c);
                    if exp == Some(c) {
                        stack.pop();
                    } else {
                        invalid = Some(c);
                        break;
                    }
                }
            },
            _ => panic!("disallowed character"),
        }
    }

    match (stack.len(), invalid) {
        (0, None) => IResult::Complete,
        (_, None) => IResult::Incomplete(stack),
        (_, Some(i)) => IResult::Corrupted(i),
    }
}

fn corrupted_line_score(line: &str) -> Option<u32> {
    match parse_line(line) {
        IResult::Complete => None,
        IResult::Corrupted(invalid_char) => match invalid_char {
            ')' => Some(3),
            ']' => Some(57),
            '}' => Some(1197),
            '>' => Some(25137),
            _ => unimplemented!(),
        },
        IResult::Incomplete(_) => None,
    }
}

fn corrupted_lines_score<'a>(lines: impl Iterator<Item = &'a str>) -> u32 {
    lines.fold(0, |acc, line| match corrupted_line_score(line) {
        Some(score) => acc + score,
        None => acc,
    })
}

fn repair_incomplete_line(mut stack: Vec<char>) -> Vec<char> {
    let pairs = match_pairs();
    stack.reverse();
    stack
        .into_iter()
        .map(|c| pairs.get(&c).map(|c| *c).unwrap())
        .collect()
}

fn score_repair(repair: Vec<char>) -> u64 {
    repair.iter().fold(0, |acc, c| {
        let sc = match c {
            ')' => 1,
            ']' => 2,
            '}' => 3,
            '>' => 4,
            _ => 0,
        };
        acc * 5 + sc
    })
}

fn score_autocomplete_contest<'a>(lines: impl Iterator<Item = &'a str>) -> u64 {
    let mut scores: Vec<u64> = lines
        .map(parse_line)
        .filter(|l| match l {
            IResult::Incomplete(_) => true,
            _ => false,
        })
        .map(|l| match l {
            IResult::Incomplete(stack) => stack.clone(),
            _ => panic!("unreachable"),
        })
        .map(repair_incomplete_line)
        .map(score_repair)
        .collect();

    scores.sort_unstable();
    let idx = scores.len() / 2;

    scores[idx]
}

pub fn day10a() -> Result<String, Error> {
    Ok(format!("day 10a: {}", corrupted_lines_score(DATA.lines())))
}

pub fn day10b() -> Result<String, Error> {
    Ok(format!(
        "day 10b: {}",
        score_autocomplete_contest(DATA.lines())
    ))
}

#[cfg(test)]
mod test {
    use super::*;
    const TEST_DATA: &str = "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";

    #[test]
    fn finds_score_for_one_line() {
        assert_eq!(corrupted_line_score("[[<[([]))<([[{}[[()]]]"), Some(3));
        assert_eq!(corrupted_line_score("[{[{({}]{}}([{[{{{}}([]"), Some(57));
        assert_eq!(corrupted_line_score("{([(<{}[<>[]}>{[]{[(<()>"), Some(1197));
        assert_eq!(corrupted_line_score("<{([([[(<>()){}]>(<<{{"), Some(25137));
    }

    #[test]
    fn calculates_corrupted_lines_score() {
        assert_eq!(corrupted_lines_score(TEST_DATA.lines()), 26397);
    }

    #[test]
    fn repairs_an_incomplete_line() {
        match parse_line("[({(<(())[]>[[{[]{<()<>>") {
            IResult::Incomplete(stack) => assert_eq!(
                repair_incomplete_line(stack),
                "}}]])})]".chars().collect::<Vec<char>>()
            ),
            _ => unimplemented!(),
        }
    }

    #[test]
    fn scores_line_repair() {
        assert_eq!(
            score_repair("}}]])})]".chars().collect::<Vec<char>>()),
            288957
        );
    }

    #[test]
    fn scores_autocomplete() {
        assert_eq!(score_autocomplete_contest(TEST_DATA.lines()), 288957);
    }
}
