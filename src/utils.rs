use std::{collections::HashMap, fmt::Debug, hash::Hash};

pub fn memoize<K, V, F>(cache: &mut HashMap<K, V>, func: F, input: K) -> V
where
    K: Eq + Hash + Copy + Debug,
    V: Copy,
    F: FnOnce(K) -> V,
{
    // *cache.entry(input).or_insert(func(input))
    let val = cache.get(&input);
    match val {
        None => {
            let res = func(input);
            cache.insert(input, res);
            res
        }
        Some(v) => *v,
    }
}
