use crate::errors::Error;
use crate::table::Table;

const DATA: &str = include_str!("../data/04.txt");

#[derive(Clone, Debug, PartialEq)]
pub struct BingoBoard {
    board: Table<(u8, bool)>,
}

impl BingoBoard {
    fn new(board: Table<(u8, bool)>) -> BingoBoard {
        BingoBoard { board }
    }

    fn val(&self, row: usize, column: usize) -> u8 {
        self.board.get(row, column).0
    }

    fn mark_number(&mut self, num: u8) {
        match self.board.get_by_mut(|(val, _)| num == *val) {
            Some(element) => element.1 = true,
            None => (),
        }
    }

    fn has_winning_row(&self) -> bool {
        self.board
            .rows()
            .iter()
            .any(|row| row.iter().all(|(_val, marked)| *marked))
    }

    fn has_winning_column(&self) -> bool {
        self.board
            .columns()
            .iter()
            .any(|row| row.iter().all(|(_val, marked)| *marked))
    }

    fn is_winning(&self) -> bool {
        self.has_winning_row() || self.has_winning_column()
    }

    fn unmarked_numbers(&self) -> Vec<u8> {
        self.board
            .elements()
            .filter(|(_val, marked)| !marked)
            .map(|(val, _)| *val)
            .collect::<Vec<u8>>()
    }

    fn as_table(&self) -> String {
        self.board
            .rows()
            .iter()
            .map(|row| {
                row.iter()
                    .map(|(val, marked)| {
                        if *marked {
                            format!("{:>3}", val)
                        } else {
                            format!("   ")
                        }
                    })
                    .collect::<Vec<String>>()
                    .join(", ")
            })
            .collect::<Vec<String>>()
            .as_slice()
            .join("\n")
    }
}

impl From<Vec<String>> for BingoBoard {
    fn from(input: Vec<String>) -> Self {
        let board = input
            .iter()
            .map(|row| {
                row.split_whitespace()
                    .map(|val| val.parse::<u8>().unwrap())
                    .map(|val| (val, false))
                    .collect::<Vec<(u8, bool)>>()
            })
            .flatten()
            .collect::<Vec<(u8, bool)>>();
        BingoBoard {
            board: Table::new(5, 5, board),
        }
    }
}

fn parse_game(input: &str) -> (Vec<u8>, Vec<BingoBoard>) {
    let data = input.lines().map(|s| s.to_owned()).collect::<Vec<String>>();

    let mut groups = data.split(|s| s.is_empty()).map(|s| s.to_owned());
    let game = groups
        .next()
        .unwrap()
        .first()
        .unwrap()
        .split(|s| s == ',')
        .map(|s| s.parse::<u8>().unwrap())
        .collect::<Vec<u8>>();

    let boards = groups
        .filter(|d| !d.is_empty())
        .map(|d| BingoBoard::from(Vec::from(d)))
        .collect::<Vec<BingoBoard>>();

    (game, boards)
}

fn play_game(game: Vec<u8>, mut boards: Vec<BingoBoard>) -> Option<(u8, BingoBoard)> {
    for num in game {
        boards.iter_mut().for_each(|board| board.mark_number(num));
        for board in boards.iter() {
            if board.is_winning() {
                return Some((num, board.clone()));
            }
        }
    }
    None
}

fn play_game_last_winner(game: Vec<u8>, mut boards: Vec<BingoBoard>) -> Option<(u8, BingoBoard)> {
    let mut most_recent_win = None;
    for num in game {
        println!("num: {}", num);
        boards.iter_mut().for_each(|board| board.mark_number(num));
        boards.iter().for_each(|b| println!("{}", b.as_table()));
        for board in boards.iter() {
            if board.is_winning() {
                most_recent_win = Some((num, board.clone()));
            }
        }
        boards = boards
            .iter()
            .filter(|b| !b.is_winning())
            .cloned()
            .collect::<Vec<BingoBoard>>();
    }
    most_recent_win
}

pub fn day4a() -> Result<String, Error> {
    let (game, boards) = parse_game(DATA);
    let result: u32 = match play_game(game, boards) {
        Some((winning_val, winning_board)) => {
            winning_val as u32
                * winning_board
                    .unmarked_numbers()
                    .iter()
                    .fold(0 as u32, |acc, val| acc + *val as u32)
        }
        None => panic!("did not get a winner"),
    };

    Ok(format!("day4a: {}", result))
}

pub fn day4b() -> Result<String, Error> {
    let (game, boards) = parse_game(DATA);
    let result: u32 = match play_game_last_winner(game, boards) {
        Some((winning_val, winning_board)) => {
            winning_val as u32
                * winning_board
                    .unmarked_numbers()
                    .iter()
                    .fold(0 as u32, |acc, val| acc + *val as u32)
        }
        None => panic!("did not get a winner"),
    };

    Ok(format!("day4b: {}", result))
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &str = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7";

    #[test]
    fn it_parses_a_board() {
        let board_str = vec![
            "22 13 17 11  0",
            " 8  2  23  4 24",
            "21  9 14 16  7",
            " 6 10  3 18  5",
            " 1 12 20 15 19",
        ]
        .iter()
        .map(|s| (*s).to_owned())
        .collect::<Vec<String>>();

        let board = BingoBoard::from(board_str);
        assert_eq!(board.val(0, 0), 22);
        assert_eq!(board.val(3, 0), 6);
        assert_eq!(board.val(1, 2), 23);
    }

    #[test]
    fn it_parses_a_game() {
        let (game, _boards) = parse_game(&TEST_DATA);
        assert_eq!(
            game,
            vec![
                7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8,
                19, 3, 26, 1
            ]
        );
    }

    #[test]
    fn it_plays_a_game() {
        let (game, boards) = parse_game(&TEST_DATA);
        let (winning_val, winning_board) = play_game(game, boards).unwrap();
        assert_eq!(winning_val, 24);
        assert_eq!(
            winning_board,
            BingoBoard::new(Table::new(
                5,
                5,
                vec![
                    (14, true),
                    (21, true),
                    (17, true),
                    (24, true),
                    (4, true),
                    (10, false),
                    (16, false),
                    (15, false),
                    (9, true),
                    (19, false),
                    (18, false),
                    (8, false),
                    (23, true),
                    (26, false),
                    (20, false),
                    (22, false),
                    (11, true),
                    (13, false),
                    (6, false),
                    (5, true),
                    (2, true),
                    (0, true),
                    (12, false),
                    (3, false),
                    (7, true),
                ]
            ))
        );
        assert_eq!(
            winning_board
                .unmarked_numbers()
                .iter()
                .fold(0, |acc, val| acc + val),
            188
        );
    }

    #[test]
    fn it_calculates_final_winner() {
        let (game, boards) = parse_game(&TEST_DATA);
        let (winning_val, winning_board) = play_game_last_winner(game, boards).unwrap();
        assert_eq!(winning_val, 13);
        assert_eq!(
            winning_board,
            BingoBoard::new(Table::new(
                5,
                5,
                vec![
                    (3, false),
                    (15, false),
                    (0, true),
                    (2, true),
                    (22, false),
                    (9, true),
                    (18, false),
                    (13, true),
                    (17, true),
                    (5, true),
                    (19, false),
                    (8, false),
                    (7, true),
                    (25, false),
                    (23, true),
                    (20, false),
                    (11, true),
                    (10, true),
                    (24, true),
                    (4, true),
                    (14, true),
                    (21, true),
                    (16, true),
                    (12, false),
                    (6, false),
                ]
            ))
        );
        assert_eq!(
            winning_board
                .unmarked_numbers()
                .iter()
                .fold(0, |acc, val| acc + val),
            148
        );
    }
}
