use crate::errors::Error;
use std::{
    collections::{HashMap, HashSet},
    ops::RangeBounds,
};

const DATA: &str = include_str!("../data/12.txt");

#[derive(Clone, PartialEq, Debug, Eq, Hash)]
enum Room {
    Start,
    End,
    Small(String),
    Large(String),
}

impl From<&str> for Room {
    fn from(input: &str) -> Room {
        match input {
            "start" => Room::Start,
            "end" => Room::End,
            id => {
                if id.chars().all(|c| c.is_uppercase()) {
                    Room::Large(id.to_owned())
                } else {
                    Room::Small(id.to_owned())
                }
            }
        }
    }
}

#[derive(Clone, Debug)]
struct Caves(HashMap<Room, HashSet<Room>>);

fn parse_data(input: &str) -> Caves {
    let mut map = HashMap::new();
    input.lines().for_each(|line| {
        let mut parts = line.split("-");
        let start = Room::from(parts.next().unwrap());
        let end = Room::from(parts.next().unwrap());

        let entry = map.entry(start.clone()).or_insert(HashSet::new());
        entry.insert(end.clone());
        let entry = map.entry(end).or_insert(HashSet::new());
        entry.insert(start);
    });

    Caves(map)
}

/* breadth-search, which is likely makes things too hard
fn paths(caves: &Caves) -> Vec<Vec<Room>> {
let system = caves.0;
let mut paths = Vec::new();
let mut visited = HashSet::new();
let mut edges = HashSet::new();

let starts = system.get(&Room::Start).unwrap();
starts.iter().for_each(|room| {
edges.insert(room);
});
while edges.len() > 0 {

}
}
*/

fn visit(
    caves: &Caves,
    mut visited: HashSet<Room>,
    max_small_duplicates: usize,
    room: Room,
    mut path: Vec<Room>,
) -> Vec<Vec<Room>> {
    path.push(room.clone());
    if room == Room::End {
        vec![path]
    } else {
        let mut paths = Vec::new();
        visited.insert(room.clone());
        let dests = caves.0.get(&room).unwrap().clone();
        for dest in dests {
            match dest {
                Room::Small(_) => {
                    if !visited.contains(&dest) {
                        paths.append(&mut visit(
                            caves,
                            visited.clone(),
                            max_small_duplicates,
                            dest,
                            path.clone(),
                        ));
                    } else if max_small_duplicates > 0 {
                        paths.append(&mut visit(
                            caves,
                            visited.clone(),
                            max_small_duplicates - 1,
                            dest,
                            path.clone(),
                        ));
                    }
                }
                Room::Large(_) => {
                    paths.append(&mut visit(
                        caves,
                        visited.clone(),
                        max_small_duplicates,
                        dest,
                        path.clone(),
                    ));
                }
                Room::End => {
                    paths.append(&mut visit(
                        caves,
                        visited.clone(),
                        max_small_duplicates,
                        dest,
                        path.clone(),
                    ));
                }
                Room::Start => {}
            }
        }
        paths
    }
}

fn paths(caves: &Caves, num_small_duplicates: usize) -> Vec<Vec<Room>> {
    visit(
        caves,
        HashSet::new(),
        num_small_duplicates,
        Room::Start,
        vec![],
    )
}

pub fn day12a() -> Result<String, Error> {
    Ok(format!("day 12a: {}", paths(&parse_data(DATA), 0).len()))
}

pub fn day12b() -> Result<String, Error> {
    Ok(format!("day 12b: {}", paths(&parse_data(DATA), 1).len()))
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_1: &str = "start-A
start-b
A-c
A-b
b-d
A-end
b-end";

    const TEST_2: &str = "dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc";

    const TEST_3: &str = "fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW";

    #[test]
    fn it_finds_path_counts() {
        assert_eq!(paths(&parse_data(TEST_1), 0).len(), 10);
        assert_eq!(paths(&parse_data(TEST_2), 0).len(), 19);
        assert_eq!(paths(&parse_data(TEST_3), 0).len(), 226);
    }

    #[test]
    fn it_allows_one_small_visit() {
        assert_eq!(paths(&parse_data(TEST_1), 1).len(), 36);
        assert_eq!(paths(&parse_data(TEST_2), 1).len(), 103);
        assert_eq!(paths(&parse_data(TEST_3), 1).len(), 3509);
    }
}
