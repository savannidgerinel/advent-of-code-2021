use crate::errors::Error;
use crate::table::Table;

const DATA: &str = include_str!("../data/13.txt");

#[derive(Clone, Debug, PartialEq)]
enum FoldI {
    Horizontal(usize),
    Vertical(usize),
}

fn overlay(table1: Table<bool>, table2: Table<bool>) -> Table<bool> {
    if table1.rows != table2.rows || table1.columns != table2.columns {
        panic!(
            "tables are not of the same dimension: {} {} {} {}",
            table1.rows, table2.rows, table1.columns, table2.columns
        );
    }
    let mut r = table1.clone();
    for row in 0..table1.rows {
        for column in 0..table1.columns {
            *r.get_mut(row, column) = *table1.get(row, column) || *table2.get(row, column);
        }
    }
    r
}

struct Page(Table<bool>);

impl Page {
    fn fold(&mut self, instruction: FoldI) {
        println!("{} {} {:?}", self.0.rows, self.0.columns, instruction);
        match instruction {
            FoldI::Horizontal(row_cnt) => {
                let (table1, table2) = self.0.split_horizontal(row_cnt);
                let table2 = table2.flip_vertical();
                let (table1, table2) = if table1.rows < table2.rows {
                    (table1.pad_top(false, table2.rows - table1.rows), table2)
                } else if table1.rows > table2.rows {
                    let rows = table1.rows;
                    (table1, table2.pad_top(false, rows - table2.rows))
                } else {
                    (table1, table2)
                };
                self.0 = overlay(table1, table2);
            }
            FoldI::Vertical(column_cnt) => {
                let (table1, table2) = self.0.split_vertical(column_cnt);
                let table2 = table2.flip_horizontal();
                let (table1, table2) = if table1.columns < table2.columns {
                    (
                        table1.pad_left(false, table2.columns - table1.columns),
                        table2,
                    )
                } else if table1.columns > table2.columns {
                    let columns = table1.columns;
                    (table1, table2.pad_left(false, columns - table2.columns))
                } else {
                    (table1, table2)
                };
                self.0 = overlay(table1, table2);
            }
        }
    }

    fn count_dots(&self) -> usize {
        self.0.elements().filter(|e| **e).count()
    }

    fn format(&self) -> String {
        self.0
            .format(|val| if *val { "#".to_owned() } else { " ".to_owned() })
    }
}

fn parse_data(input: &str) -> (Page, Vec<FoldI>) {
    let dot_iter = input
        .lines()
        .map(|line| line.trim())
        .take_while(|line| *line != "");
    let dots: Vec<(usize, usize)> = dot_iter
        .map(|line| {
            let parts = line.split(",").collect::<Vec<&str>>();
            (
                parts[0].parse::<usize>().unwrap(),
                parts[1].parse::<usize>().unwrap(),
            )
        })
        .collect();

    let max_column: usize = *dots.iter().map(|(column, _)| column).max().unwrap();
    let max_row: usize = *dots.iter().map(|(_, row)| row).max().unwrap();

    let mut t = Table::new(
        max_row + 1,
        max_column + 1,
        vec![false; (max_row + 1) * (max_column + 1)],
    );
    dots.into_iter()
        .for_each(|(column, row)| *t.get_mut(row, column) = true);

    let instrs = input
        .lines()
        .map(|line| line.trim())
        .skip_while(|line| *line != "")
        .skip(1)
        .map(|line| line.chars().skip(11).collect::<String>())
        .map(|line| {
            let parts = line.split("=").collect::<Vec<&str>>();
            let idx = parts[1].parse::<usize>().unwrap();
            match parts[0] {
                "x" => FoldI::Vertical(idx),
                "y" => FoldI::Horizontal(idx),
                _ => panic!("unvalid char: {}", parts[0]),
            }
        })
        .collect();

    (Page(t), instrs)
}

pub fn day13a() -> Result<String, Error> {
    let (mut page, instrs) = parse_data(DATA);
    page.fold(instrs[0].clone());
    println!("{}", page.format());
    Ok(format!("day 13a: {}", page.count_dots()))
}

pub fn day13b() -> Result<String, Error> {
    let (mut page, instrs) = parse_data(DATA);
    instrs.into_iter().for_each(|instr| page.fold(instr));
    Ok(format!("day 13b:\n{}", page.format()))
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &str = "6,10
        0,14
        9,10
        0,3
        10,4
        4,11
        6,0
        6,12
        4,1
        0,13
        10,12
        3,4
        3,0
        8,4
        1,10
        2,14
        8,10
        9,0

        fold along y=7
        fold along x=5";

    #[test]
    fn parses_test_data() {
        let (page, instrs) = parse_data(TEST_DATA);
        assert_eq!(page.0.rows, 15);
        assert_eq!(page.0.columns, 11);
        assert_eq!(instrs, [FoldI::Horizontal(7), FoldI::Vertical(5)]);
    }

    #[test]
    fn counts_dots_after_horizontal_fold() {
        let (mut page, _) = parse_data(TEST_DATA);
        page.fold(FoldI::Horizontal(7));
        assert_eq!(page.count_dots(), 17);
    }

    #[test]
    fn counts_dots_after_double_fold() {
        let (mut page, _) = parse_data(TEST_DATA);
        page.fold(FoldI::Horizontal(7));
        page.fold(FoldI::Vertical(5));
        println!("{}", page.format());
        assert_eq!(page.count_dots(), 16);
    }
}
