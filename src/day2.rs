use crate::errors::Error;

const DATA: &str = include_str!("../data/02.txt");

fn instructions() -> Vec<Instruction> {
    DATA.lines()
        .map(Instruction::from)
        .collect::<Vec<Instruction>>()
}

#[derive(Clone, Debug, PartialEq)]
enum Instruction {
    Forward(u32),
    Down(u32),
    Up(u32),
}

impl From<&str> for Instruction {
    fn from(s: &str) -> Instruction {
        let mut words = s.split_whitespace();
        let direction = words.next().unwrap();
        let distance = words.next().unwrap().parse::<u32>().unwrap();
        match direction {
            "forward" => Instruction::Forward(distance),
            "down" => Instruction::Down(distance),
            "up" => Instruction::Up(distance),
            _ => unreachable!(),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
struct Position {
    distance: u32,
    depth: u32,
}

impl Position {
    fn move_(&self, dir: Instruction) -> Position {
        match dir {
            Instruction::Forward(d) => Position {
                distance: self.distance + d,
                depth: self.depth,
            },
            Instruction::Down(d) => Position {
                distance: self.distance,
                depth: self.depth + d,
            },
            Instruction::Up(d) => Position {
                distance: self.distance,
                depth: self.depth - d,
            },
        }
    }
}

fn move_submarine(instructions: Vec<Instruction>) -> Position {
    let position = Position {
        distance: 0,
        depth: 0,
    };
    instructions
        .into_iter()
        .fold(position, |position, i| position.move_(i))
}

#[derive(Clone, Debug, PartialEq)]
struct AimedPosition {
    distance: u32,
    depth: u32,
    aim: u32,
}

impl AimedPosition {
    fn move_(&self, dir: Instruction) -> AimedPosition {
        match dir {
            Instruction::Forward(d) => AimedPosition {
                distance: self.distance + d,
                depth: self.depth + d * self.aim,
                aim: self.aim,
            },
            Instruction::Down(d) => AimedPosition {
                distance: self.distance,
                depth: self.depth,
                aim: self.aim + d,
            },
            Instruction::Up(d) => AimedPosition {
                distance: self.distance,
                depth: self.depth,
                aim: self.aim - d,
            },
        }
    }
}

fn aimed_submarine(instructions: Vec<Instruction>) -> AimedPosition {
    let position = AimedPosition {
        distance: 0,
        depth: 0,
        aim: 0,
    };
    instructions
        .into_iter()
        .fold(position, |position, i| position.move_(i))
}

pub fn day2a() -> Result<String, Error> {
    let position = move_submarine(instructions());

    Ok(format!(
        "Final position: [{}] distance {}, depth {}",
        position.distance * position.depth,
        position.distance,
        position.depth
    ))
}

pub fn day2b() -> Result<String, Error> {
    let position = aimed_submarine(instructions());

    Ok(format!(
        "Final position: [{}] distance {}, depth {}, aim {}",
        position.distance * position.depth,
        position.distance,
        position.depth,
        position.aim
    ))
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &str = "forward 5
down 5
forward 8
up 3
down 8
forward 2";

    #[test]
    fn it_parses_rows() {
        let instructions = TEST_DATA
            .lines()
            .map(Instruction::from)
            .collect::<Vec<Instruction>>();
        assert_eq!(
            instructions,
            vec![
                Instruction::Forward(5),
                Instruction::Down(5),
                Instruction::Forward(8),
                Instruction::Up(3),
                Instruction::Down(8),
                Instruction::Forward(2)
            ]
        );
    }

    #[test]
    fn it_calculates_position() {
        let instructions = TEST_DATA
            .lines()
            .map(Instruction::from)
            .collect::<Vec<Instruction>>();
        assert_eq!(
            move_submarine(instructions),
            Position {
                distance: 15,
                depth: 10
            }
        );
    }

    #[test]
    fn it_calculates_aimed_position() {
        let instructions = TEST_DATA
            .lines()
            .map(Instruction::from)
            .collect::<Vec<Instruction>>();
        assert_eq!(
            aimed_submarine(instructions),
            AimedPosition {
                distance: 15,
                depth: 60,
                aim: 10
            }
        );
    }
}
