use crate::errors::Error;
use crate::table::Table;
use nom::{
    bytes::complete::tag,
    character::{complete::digit1, streaming::char},
    sequence::separated_pair,
    IResult,
};

const DATA: &str = include_str!("../data/05.txt");

#[derive(Clone, Copy, Debug, PartialEq)]
struct Point {
    x: usize,
    y: usize,
}

fn point(input: &str) -> IResult<&str, Point> {
    let (input, (x, y)) = separated_pair(digit1, char(','), digit1)(input)?;
    Ok((
        input,
        Point {
            x: x.trim().parse::<usize>().unwrap(),
            y: y.trim().parse::<usize>().unwrap(),
        },
    ))
}

#[derive(Clone, Debug, PartialEq)]
struct Line(Point, Point);

fn line(input: &str) -> IResult<&str, Line> {
    let (input, (point1, point2)) = separated_pair(point, tag(" -> "), point)(input)?;
    Ok((input, Line(point1, point2)))
}

impl From<&str> for Line {
    fn from(input: &str) -> Line {
        let (_, line) = line(input).unwrap();
        line
    }
}

fn parse_input(input: &str) -> Vec<Line> {
    input
        .lines()
        .map(|line| Line::from(line))
        .collect::<Vec<Line>>()
}

struct Image {
    bitmap: Table<u32>,
}

impl Image {
    fn new(rows: usize, columns: usize) -> Image {
        let mut pixels = Vec::new();
        (0..(rows * columns)).for_each(|_| pixels.push(0));
        Image {
            bitmap: Table::new(rows, columns, pixels),
        }
    }

    fn draw_line(&mut self, line: &Line) {
        let steps = if line.0.x == line.1.x {
            ((line.1.y as i32) - (line.0.y as i32)).abs() as usize
        } else {
            ((line.1.x as i32) - (line.0.x as i32)).abs() as usize
        };
        let cmp = |start, end| {
            if end > start {
                1
            } else if start > end {
                -1
            } else {
                0
            }
        };
        let dx: i32 = cmp(line.0.x, line.1.x);
        let dy: i32 = cmp(line.0.y, line.1.y);
        (0..steps + 1).for_each(|step| {
            let x = (line.0.x as i32) + (dx * step as i32);
            let y = (line.0.y as i32) + (dy * step as i32);
            let pixel = self.bitmap.get_mut(y as usize, x as usize);
            *pixel += 1;
        })
    }

    fn count_overlap(&self) -> usize {
        self.bitmap.elements().filter(|v| **v > 1).count()
    }

    fn render(&self) -> String {
        self.bitmap
            .rows()
            .iter()
            .map(|row| {
                row.iter()
                    .map(|v| {
                        if *v == 0 {
                            ".".to_owned()
                        } else {
                            format!("{}", v)
                        }
                    })
                    .collect::<String>()
            })
            .collect::<Vec<String>>()
            .join("\n")
    }
}

pub fn day5a() -> Result<String, Error> {
    let mut image = Image::new(1000, 1000);
    parse_input(DATA)
        .iter()
        .filter(|line| line.0.x == line.1.x || line.0.y == line.1.y)
        .for_each(|line| image.draw_line(line));

    Ok(format!("day5a: {}", image.count_overlap()))
}

pub fn day5b() -> Result<String, Error> {
    let mut image = Image::new(1000, 1000);
    parse_input(DATA)
        .iter()
        .for_each(|line| image.draw_line(line));

    Ok(format!("day5b: {}", image.count_overlap()))
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &str = "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2";

    #[test]
    fn it_parses_data_set() {
        let lines = parse_input(TEST_DATA);
        assert_eq!(lines.len(), 10);
        assert_eq!(lines[0], Line(Point { x: 0, y: 9 }, Point { x: 5, y: 9 }));
        assert_eq!(lines[9], Line(Point { x: 5, y: 5 }, Point { x: 8, y: 2 }));
    }

    #[test]
    fn it_draws_only_horizontal_and_vertical_lines() {
        let mut image = Image::new(10, 10);

        parse_input(TEST_DATA)
            .iter()
            .filter(|line| line.0.x == line.1.x || line.0.y == line.1.y)
            .for_each(|line| image.draw_line(line));

        println!("{}", image.render());
        assert_eq!(image.count_overlap(), 5);
    }

    #[test]
    fn it_draws_all_lines() {
        let mut image = Image::new(10, 10);

        parse_input(TEST_DATA)
            .iter()
            .for_each(|line| image.draw_line(line));

        println!("{}", image.render());
        assert_eq!(image.count_overlap(), 12);
    }
}
