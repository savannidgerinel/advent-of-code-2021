use crate::errors::Error;
use crate::table::Table;

const DATA: &str = include_str!("../data/11.txt");

struct Map(Table<(bool, u32)>);

impl Map {
    fn tick(&mut self) -> u32 {
        self.0.elements_mut().for_each(|e| e.1 += 1);

        loop {
            let mut loop_flashes = 0;
            for row in 0..self.0.rows {
                for column in 0..self.0.columns {
                    let o = self.0.get_mut(row, column);
                    if o.1 >= 10 && !o.0 {
                        loop_flashes += 1;
                        o.0 = true;
                        o.1 += 1;
                        self.inc_adjacents(row, column);
                    }
                }
            }
            if loop_flashes == 0 {
                break;
            }
        }

        let flashes = self.0.elements().filter(|e| e.0).count();
        self.0.elements_mut().for_each(|e| {
            if e.0 {
                *e = (false, 0);
            }
        });
        flashes as u32
    }

    fn inc_adjacents(&mut self, row: usize, column: usize) {
        for dr in [-1, 0, 1] {
            for dc in [-1, 0, 1] {
                let row_ = row as i32 + dr;
                let column_ = column as i32 + dc;
                if row_ >= 0
                    && row_ < self.0.rows as i32
                    && column_ >= 0
                    && column_ < self.0.columns as i32
                {
                    self.0.get_mut(row_ as usize, column_ as usize).1 += 1;
                }
            }
        }
    }

    fn format(&self) -> String {
        self.0
            .rows()
            .iter()
            .map(|row| {
                row.iter()
                    .map(|c| {
                        if c.0 {
                            "**".to_owned()
                        } else {
                            format!("{:2}", c.1)
                        }
                    })
                    .collect::<Vec<String>>()
                    .join(" ")
            })
            .collect::<Vec<String>>()
            .join("\n")
    }
}

fn parse_data(input: &str) -> Map {
    let rows = input.lines().count();
    let columns = input.lines().next().unwrap().chars().count();
    let octopi = input
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| c.to_digit(10).unwrap())
                .collect::<Vec<u32>>()
        })
        .flatten()
        .map(|energy| (false, energy))
        .collect::<Vec<(bool, u32)>>();

    Map(Table::new(rows, columns, octopi))
}

fn synchronized_flash(octopi: &mut Map) -> u32 {
    let octopus_count = octopi.0.rows * octopi.0.columns;
    for i in 0.. {
        if i % 10 == 0 {
            println!("iteration: {}", i);
        }
        if octopi.tick() == octopus_count as u32 {
            return i + 1;
        }
    }
    unimplemented!()
}

pub fn day11a() -> Result<String, Error> {
    let mut octopi = parse_data(DATA);
    let flashes = (0..100).fold(0, |flashes, _| flashes + octopi.tick());
    Ok(format!("day11a: {}", flashes))
}

pub fn day11b() -> Result<String, Error> {
    let mut octopi = parse_data(DATA);
    Ok(format!("day 11b: {}", synchronized_flash(&mut octopi)))
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &str = "5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";

    const TRIVIAL_DATA: &str = "11111
11111
11911
11111
11111";

    const RECURSIVE_DATA: &str = "11111
19991
19191
19991
11111";

    #[test]
    fn solves_trivial_case() {
        let mut octopi = parse_data(TRIVIAL_DATA);
        let flashes = octopi.tick();
        assert_eq!(flashes, 1);
    }

    #[test]
    fn solves_recursive_case() {
        let mut octopi = parse_data(RECURSIVE_DATA);
        let flashes = octopi.tick();
        assert_eq!(flashes, 9);
    }

    #[test]
    fn solves_demo_case() {
        let mut octopi = parse_data(TEST_DATA);
        /*
        assert_eq!(octopi.tick(), 0);
        println!("-----");
        assert_eq!(octopi.tick(), 35);
        println!("-----");
        assert_eq!(octopi.tick(), 45);
        */
        let flashes = (0..10).fold(0, |flashes, _| flashes + octopi.tick());
        assert_eq!(flashes, 204);
    }

    #[test]
    fn solves_100_steps() {
        let mut octopi = parse_data(TEST_DATA);
        let flashes = (0..100).fold(0, |flashes, _| flashes + octopi.tick());
        assert_eq!(flashes, 1656);
    }

    #[test]
    fn finds_first_synchronized_flash() {
        let mut octopi = parse_data(TEST_DATA);
        assert_eq!(synchronized_flash(&mut octopi), 195);
    }
}
