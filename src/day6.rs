use crate::errors::Error;

const DATA: &str = include_str!("../data/06.txt");

/*
fn decrement(input: &Vec<u8>) -> Vec<u8> {
input
.iter()
.map(|v| if *v == 0 { 6 } else { *v - 1 })
.collect::<Vec<u8>>()
}

fn new_fish(input: &Vec<u8>) -> Vec<u8> {
let births = input.iter().filter(|v| **v == 0).count();
(0..births).map(|_| 8).collect::<Vec<u8>>()
}

fn lanternfish_cycle(input: Vec<u8>, days: u32) -> Vec<u8> {
(0..days).fold(input, |current, day| {
let mut births = new_fish(&current);
let mut aged = decrement(&current);
aged.append(&mut births);
aged
})
}
*/

fn step(input: &Vec<u64>) -> Vec<u64> {
    let reproducing = input[0];
    let mut output = input.iter().skip(1).cloned().collect::<Vec<u64>>();
    output.push(reproducing);
    output[6] += reproducing;
    output
}

fn lanternfish_cycle(input: Vec<u64>, days: u64) -> Vec<u64> {
    assert!(input.len() == 9);

    (0..days).fold(input, |current, _| {
        let next = step(&current);
        next
    })
}

fn num_lanternfish(input: Vec<u64>) -> u64 {
    input.iter().fold(0, |acc, val| acc + (*val as u64))
}

fn collate(input: Vec<u8>) -> Vec<u64> {
    let mut counts = vec![0, 0, 0, 0, 0, 0, 0, 0, 0];

    input
        .iter()
        .for_each(|val| counts[*val as usize] = counts[*val as usize] + 1);

    counts
}

pub fn day6a() -> Result<String, Error> {
    let input = DATA
        .split(",")
        .map(|s| s.trim().parse::<u8>().unwrap())
        .collect();
    let count = num_lanternfish(lanternfish_cycle(collate(input), 80));
    Ok(format!("day6a: {}", count))
}

pub fn day6b() -> Result<String, Error> {
    let input = DATA
        .split(",")
        .map(|s| s.trim().parse::<u8>().unwrap())
        .collect();
    let count = num_lanternfish(lanternfish_cycle(collate(input), 256));
    Ok(format!("day6b: {}", count))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn solves_18_days() {
        let input: Vec<u8> = vec![3, 4, 3, 1, 2];
        assert_eq!(num_lanternfish(lanternfish_cycle(collate(input), 18)), 26);
    }

    #[test]
    fn solves_80_days() {
        let input: Vec<u8> = vec![3, 4, 3, 1, 2];
        assert_eq!(num_lanternfish(lanternfish_cycle(collate(input), 80)), 5934);
    }

    #[test]
    fn solves_256_days() {
        let input: Vec<u8> = vec![3, 4, 3, 1, 2];
        assert_eq!(
            num_lanternfish(lanternfish_cycle(collate(input), 256)),
            26984457539
        );
    }
}
