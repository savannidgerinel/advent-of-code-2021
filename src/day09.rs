use crate::{errors::Error, table::Table};

const DATA: &str = include_str!("../data/09.txt");

struct Floor(Table<u32>);

impl Floor {
    fn get(&self, row: usize, column: usize) -> u32 {
        *self.0.get(row, column)
    }

    fn is_local_min(&self, row: usize, column: usize) -> bool {
        let val = self.0.get(row, column);
        let adjacent = self.0.adjacencies(row, column);
        adjacent.iter().all(|adj| val < adj)
    }

    fn all_local_mins(&self) -> Vec<(usize, usize)> {
        let mut mins = Vec::new();
        for row in 0..self.0.rows {
            for column in 0..self.0.columns {
                if self.is_local_min(row, column) {
                    mins.push((row, column));
                }
            }
        }
        mins
    }

    fn adj_addrs(&self, row: usize, column: usize) -> Vec<(usize, usize)> {
        let mut adj = vec![];
        if row > 0 {
            adj.push((row - 1, column));
        }
        if column > 0 {
            adj.push((row, column - 1));
        }
        if row < self.0.rows - 1 {
            adj.push((row + 1, column));
        }
        if column < self.0.columns - 1 {
            adj.push((row, column + 1));
        }
        adj
    }

    fn basin(&self, row: usize, column: usize) -> usize {
        let mut visited = vec![(row, column)];
        let mut fringes = self
            .adj_addrs(row, column)
            .iter()
            .filter(|(r, c)| self.get(*r, *c) < 9)
            .cloned()
            .collect::<Vec<(usize, usize)>>();

        while fringes.len() > 0 {
            let (r, c) = fringes.remove(0);
            let neighbors = self
                .adj_addrs(r, c)
                .iter()
                .filter(|(r, c)| self.get(*r, *c) < 9)
                .cloned()
                .collect::<Vec<(usize, usize)>>();
            for n in neighbors {
                if !visited.contains(&n) && !fringes.contains(&n) {
                    fringes.push(n);
                }
            }
            visited.push((r, c))
        }

        visited.len()
    }

    fn basins(&self) -> Vec<usize> {
        let mut basins = self
            .all_local_mins()
            .iter()
            .map(|(row, column)| self.basin(*row, *column))
            .collect::<Vec<usize>>();
        basins.sort_unstable();
        basins.reverse();
        basins
    }
}

fn data(input: &str) -> Floor {
    Floor(Table::from(
        input
            .lines()
            .map(|l| {
                l.chars()
                    .map(|c| c.to_digit(10).unwrap())
                    .collect::<Vec<u32>>()
            })
            .collect::<Vec<Vec<u32>>>(),
    ))
}

fn calculate_risk(floor: &Floor) -> u32 {
    floor
        .all_local_mins()
        .iter()
        .fold(0, |acc, (row, column)| acc + (floor.get(*row, *column) + 1))
}

fn largest_three_basins(floor: &Floor) -> u32 {
    floor
        .basins()
        .iter()
        .take(3)
        .fold(1, |acc, basin_size| acc * *basin_size as u32)
}

pub fn day09a() -> Result<String, Error> {
    let floor = data(DATA);
    Ok(format!("day9a: {}", calculate_risk(&floor)))
}

pub fn day09b() -> Result<String, Error> {
    let floor = data(DATA);
    Ok(format!("day9b: {}", largest_three_basins(&floor)))
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_DATA: &str = "2199943210
3987894921
9856789892
8767896789
9899965678";

    #[test]
    fn finds_local_mins() {
        let floor = data(TEST_DATA);
        assert_eq!(calculate_risk(&floor), 15);
    }

    #[test]
    fn finds_basins() {
        let floor = data(TEST_DATA);
        assert_eq!(floor.basins(), vec![14, 9, 9, 3]);
        assert_eq!(largest_three_basins(&floor), 1134)
    }
}
