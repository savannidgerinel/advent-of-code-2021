#[derive(Clone, Debug, PartialEq)]
pub struct Table<T: Clone> {
    pub rows: usize,
    pub columns: usize,
    elements: Vec<T>,
}

impl<T: Clone> Table<T> {
    pub fn new(rows: usize, columns: usize, elements: Vec<T>) -> Table<T> {
        if rows * columns != elements.len() {
            panic!("size of the elements doesn't match rows and columns");
        }
        Table {
            rows,
            columns,
            elements,
        }
    }

    pub fn get<'a>(&'a self, row: usize, column: usize) -> &'a T {
        self.elements.get(self.address(row, column)).unwrap()
    }

    pub fn adjacencies<'a>(&'a self, row: usize, column: usize) -> Vec<&'a T> {
        let mut adjs = Vec::new();
        if row > 0 {
            adjs.push(self.get(row - 1, column));
        }
        if row < self.rows - 1 {
            adjs.push(self.get(row + 1, column));
        }
        if column > 0 {
            adjs.push(self.get(row, column - 1));
        }
        if column < self.columns - 1 {
            adjs.push(self.get(row, column + 1));
        }
        adjs
    }

    pub fn get_by<'a, F>(&'a mut self, f: F) -> Option<&'a T>
    where
        F: Fn(&T) -> bool,
    {
        self.elements.iter().find(|val| f(val))
    }

    pub fn get_mut<'a>(&'a mut self, row: usize, column: usize) -> &'a mut T {
        let address = self.address(row, column);
        self.elements.get_mut(address).unwrap()
    }

    pub fn get_by_mut<'a, F>(&'a mut self, f: F) -> Option<&'a mut T>
    where
        F: Fn(&T) -> bool,
    {
        self.elements.iter_mut().find(|val| f(val))
    }

    fn address(&self, row: usize, column: usize) -> usize {
        if (column >= self.columns) || (row > self.rows) {
            panic!(
                "coordinate is outside of the limits: [{} {}] {} {}",
                self.rows, self.columns, row, column
            );
        }
        row * self.columns + column
    }

    pub fn row(&self, row: usize) -> Vec<T> {
        (0..(self.columns))
            .map(|column| self.get(row, column).clone())
            .collect::<Vec<T>>()
    }

    pub fn rows(&self) -> Vec<Vec<T>> {
        (0..(self.rows))
            .map(|row| self.row(row))
            .collect::<Vec<Vec<T>>>()
    }

    pub fn column(&self, column: usize) -> Vec<T> {
        (0..(self.rows))
            .map(|row| self.get(row, column).clone())
            .collect::<Vec<T>>()
    }

    pub fn columns(&self) -> Vec<Vec<T>> {
        (0..(self.columns))
            .map(|columns| self.column(columns))
            .collect::<Vec<Vec<T>>>()
    }

    pub fn elements<'a>(&'a self) -> impl Iterator<Item = &'a T> {
        self.elements.iter()
    }

    pub fn elements_mut<'a>(&'a mut self) -> impl Iterator<Item = &'a mut T> {
        self.elements.iter_mut()
    }

    pub fn split_horizontal(&self, row: usize) -> (Table<T>, Table<T>) {
        let table1 = Table::from(self.rows().into_iter().take(row).collect::<Vec<Vec<T>>>());
        let table2 = Table::from(
            self.rows()
                .into_iter()
                .skip(row + 1)
                .collect::<Vec<Vec<T>>>(),
        );
        (table1, table2)
    }

    pub fn flip_vertical(&self) -> Table<T> {
        let mut rows = self.rows();
        rows.reverse();
        Table::from(rows)
    }

    pub fn split_vertical(&self, column: usize) -> (Table<T>, Table<T>) {
        let mut table1 = vec![None; self.rows * column];
        let mut table2 = vec![None; self.rows * (self.columns - column - 1)];
        for r_idx in 0..self.rows {
            for c_idx in 0..self.columns {
                if c_idx < column {
                    table1[addr(column, r_idx, c_idx)] = Some(self.get(r_idx, c_idx).clone());
                } else if c_idx > column {
                    table2[addr(self.columns - column - 1, r_idx, c_idx - column - 1)] =
                        Some(self.get(r_idx, c_idx).clone());
                }
            }
        }

        (
            Table::new(
                self.rows,
                column,
                table1.into_iter().map(|v| v.unwrap()).collect(),
            ),
            Table::new(
                self.rows,
                self.columns - column - 1,
                table2.into_iter().map(|v| v.unwrap()).collect(),
            ),
        )
    }

    pub fn flip_horizontal(&self) -> Table<T> {
        let mut table = vec![None; self.rows * self.columns];
        for r_idx in 0..self.rows {
            for c_idx in 0..self.columns {
                table[addr(self.columns, r_idx, self.columns - c_idx - 1)] =
                    Some(self.get(r_idx, c_idx).clone());
            }
        }
        Table::new(
            self.rows,
            self.columns,
            table.into_iter().map(|v| v.unwrap()).collect(),
        )
    }

    pub fn pad_left(&self, value: T, columns: usize) -> Table<T> {
        let mut t = Table::new(
            self.rows,
            self.columns + columns,
            vec![value; self.rows * (self.columns + columns)],
        );
        for r_idx in 0..t.rows {
            for c_idx in 0..t.columns {
                if c_idx >= columns {
                    *t.get_mut(r_idx, c_idx) =
                        self.elements[self.address(r_idx, c_idx - columns)].clone()
                }
            }
        }
        t
    }

    pub fn pad_top(&self, value: T, rows: usize) -> Table<T> {
        let mut t = Table::new(
            self.rows + rows,
            self.columns,
            vec![value; (self.rows + rows) * self.columns],
        );
        for r_idx in 0..t.rows {
            for c_idx in 0..t.columns {
                if r_idx >= rows {
                    *t.get_mut(r_idx, c_idx) =
                        self.elements[self.address(r_idx - rows, c_idx)].clone()
                }
            }
        }
        t
    }

    pub fn pad_right(&self, value: T, columns: usize) -> Table<T> {
        let mut t = Table::new(
            self.rows,
            self.columns + columns,
            vec![value; self.rows * (self.columns + columns)],
        );
        for r_idx in 0..t.rows {
            for c_idx in 0..t.columns {
                if c_idx < self.columns {
                    *t.get_mut(r_idx, c_idx) = self.elements[self.address(r_idx, c_idx)].clone()
                }
            }
        }
        t
    }

    pub fn pad_bottom(&self, value: T, rows: usize) -> Table<T> {
        let mut t = Table::new(
            self.rows + rows,
            self.columns,
            vec![value; (self.rows + rows) * self.columns],
        );
        for r_idx in 0..t.rows {
            for c_idx in 0..t.columns {
                if r_idx < self.rows {
                    *t.get_mut(r_idx, c_idx) = self.elements[self.address(r_idx, c_idx)].clone()
                }
            }
        }
        t
    }

    pub fn format(&self, f: impl Fn(&T) -> String) -> String {
        (0..self.rows)
            .map(|r_idx| {
                (0..self.columns)
                    .map(|c_idx| f(self.get(r_idx, c_idx)))
                    .collect::<String>()
            })
            .collect::<Vec<String>>()
            .join("\n")
    }
}

impl<T: Clone> From<Vec<Vec<T>>> for Table<T> {
    fn from(elements: Vec<Vec<T>>) -> Table<T> {
        let rows = elements.len();
        let columns = elements.iter().fold(elements[0].len(), |acc, row| {
            if acc == row.len() {
                acc
            } else {
                panic!("rows of the incoming vec must all have the same length")
            }
        });
        Table {
            rows,
            columns,
            elements: elements.iter().flatten().cloned().collect::<Vec<T>>(),
        }
    }
}

pub fn addr(columns: usize, r_idx: usize, c_idx: usize) -> usize {
    r_idx * columns + c_idx
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn can_split_horizontally() {
        let table = Table::new(10, 14, vec![false; 10 * 14]);
        let (t1, t2) = table.split_horizontal(4);
        assert_eq!(t1.rows, 4);
        assert_eq!(t1.columns, 14);
        assert_eq!(t2.rows, 5);
        assert_eq!(t2.columns, 14);
    }

    #[test]
    fn can_split_vertically() {
        let table = Table::new(10, 14, vec![false; 10 * 14]);
        let (t1, t2) = table.split_vertical(4);
        assert_eq!(t1.rows, 10);
        assert_eq!(t1.columns, 4);
        assert_eq!(t2.rows, 10);
        assert_eq!(t2.columns, 9);
    }

    #[test]
    fn can_pad_left() {
        let mut table = Table::new(5, 5, vec![false; 5 * 5]);
        *table.get_mut(0, 0) = true;
        let t2 = table.pad_left(false, 2);
        assert_eq!(*t2.get(0, 0), false);
        assert_eq!(*t2.get(0, 2), true);
    }

    #[test]
    fn can_pad_right() {
        let mut table = Table::new(5, 5, vec![false; 5 * 5]);
        *table.get_mut(0, 0) = true;
        let t2 = table.pad_right(true, 2);
        assert_eq!(*t2.get(0, 0), true);
        assert_eq!(*t2.get(0, 3), false);
        assert_eq!(*t2.get(0, 6), true);
    }

    #[test]
    fn can_pad_top() {
        let mut table = Table::new(5, 5, vec![false; 5 * 5]);
        *table.get_mut(0, 0) = true;
        let t2 = table.pad_top(false, 2);
        assert_eq!(*t2.get(0, 0), false);
        assert_eq!(*t2.get(2, 0), true);
    }

    #[test]
    fn can_pad_bottom() {
        let mut table = Table::new(5, 5, vec![false; 5 * 5]);
        *table.get_mut(0, 0) = true;
        let t2 = table.pad_bottom(true, 2);
        assert_eq!(*t2.get(0, 0), true);
        assert_eq!(*t2.get(3, 0), false);
        assert_eq!(*t2.get(6, 0), true);
    }
}
