use crate::errors::Error;
use crate::utils::memoize;
use std::{collections::HashMap, fmt::Debug, hash::Hash};

const DATA: &str = include_str!("../data/07.txt");

fn data() -> Vec<u32> {
    DATA.split(",")
        .map(|v| v.trim().parse::<u32>().unwrap())
        .collect::<Vec<u32>>()
}

fn gas_expenditure(data: &Vec<u32>, dest: u32, _cache: &mut HashMap<u32, u32>) -> u32 {
    data.iter()
        .map(|pos| (*pos as i32 - dest as i32).abs() as u32)
        .sum()
}

fn crab_gas_(distance: u32, cache: &mut HashMap<u32, u32>) -> u32 {
    // println!("crab_gas: {}", distance);
    memoize(cache, |d| (1..d + 1).sum(), distance)
    // memoize(cache, |d| (d * (d + 1)) / 2, distance)
}

fn crab_gas_expenditure(data: &Vec<u32>, dest: u32, cache: &mut HashMap<u32, u32>) -> u32 {
    data.iter()
        .map(|pos| {
            let distance = (*pos as i32 - dest as i32).abs() as u32;
            crab_gas_(distance, cache)
        })
        .sum()
}

fn find_minimum<F>(data: &Vec<u32>, fuel_function: F) -> (u32, u32)
where
    F: Fn(&Vec<u32>, u32, &mut HashMap<u32, u32>) -> u32,
{
    let mut cache = HashMap::new();
    let min_x = data.iter().min().unwrap();
    let max_x = data.iter().max().unwrap();

    (*min_x..*max_x)
        .map(|x| (x, fuel_function(data, x, &mut cache)))
        .min_by(|(_, fuel1), (_, fuel2)| fuel1.cmp(fuel2))
        .unwrap()
}

pub fn day07a() -> Result<String, Error> {
    let (_, gas) = find_minimum(&data(), gas_expenditure);
    Ok(format!("day 7a: {}", gas))
}

pub fn day07b() -> Result<String, Error> {
    let (_, gas) = find_minimum(&data(), crab_gas_expenditure);
    Ok(format!("day 7b: {}", gas))
}

#[cfg(test)]
mod test {
    use super::*;

    fn test_data() -> Vec<u32> {
        vec![16, 1, 2, 0, 4, 2, 7, 1, 2, 14]
    }

    #[test]
    fn it_balances_the_numbers() {
        let minimum = find_minimum(&test_data(), gas_expenditure);
        assert_eq!(minimum, (2, 37));
    }

    #[test]
    fn it_balances_with_crab_engineering() {
        let minimum = find_minimum(&test_data(), crab_gas_expenditure);
        assert_eq!(minimum, (5, 168));
    }
}
