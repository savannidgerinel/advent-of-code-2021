let
    rust_overlay = import (builtins.fetchTarball "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
    pkgs = import <pkgs-21.05> { overlays = [ rust_overlay ]; };
    unstable = import <unstable> {};
    rust = pkgs.rust-bin.stable."1.55.0".default.override {
      extensions = [ "rust-src" ];
    };

in pkgs.mkShell {
    name = "datasphere";

    nativeBuildInputs = [
      pkgs.gnome.webkitgtk
      pkgs.glib
      pkgs.gtk3
      pkgs.libpng
      pkgs.openssl
      pkgs.pkg-config
      pkgs.wrapGAppsHook
      rust
      unstable.rust-analyzer
    ];

    shellHook = ''
      if [ -e ~/.nixpkgs/shellhook.sh ]; then . ~/.nixpkgs/shellhook.sh; fi
    '';
}
